from django.urls import path
from tasks.views import show_project, create_task, show_my_tasks


urlpatterns = [
    path("<int:id>/", show_project, name="show_project"),
    path("create/", create_task, name="create_task"),
    path("mine/", show_my_tasks, name="show_my_tasks"),
]
