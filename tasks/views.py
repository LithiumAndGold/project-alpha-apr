from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from tasks.models import Task, Project
from tasks.forms import CreateTaskForm


@login_required
def show_project(request, id):
    task = get_object_or_404(Project, id=id)
    context = {"task_object": task}
    return render(request, "tasks/showproject.html", context)


@login_required
def create_task(request):
    if request.method == "POST":
        form = CreateTaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateTaskForm()
    context = {"form": form}
    return render(request, "projects/createproject.html", context)


@login_required
def show_my_tasks(request):
    task = Task.objects.filter(assignee=request.user)
    context = {"show_my_tasks": task}
    return render(request, "tasks/mytasks.html", context)
